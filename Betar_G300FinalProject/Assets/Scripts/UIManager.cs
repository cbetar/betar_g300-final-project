﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject petManager;
    private PetManager pM;

    public GameObject fDrop; //Feeding Drop-Down Menu
    private bool selectingFood;

    public GameObject tDrop; //Toys Drop-Down Menu
    private bool selectingToy;

    public void Start()
    {
        pM = petManager.GetComponent<PetManager>();
        
        fDrop.SetActive(false); //make sure drop down is not active at the start
        selectingFood = false;

        tDrop.SetActive(false);
        selectingToy = false;

    }

    public void feedDropDown()
    {
        //If the menu is already open, close it.
        if (fDrop.activeSelf == true)
        {
            fDrop.SetActive(false);
            pM.feedPet();
            selectingFood = false;
        }
        //If the toy menu is not opened and the button is pressed, make the menu active
        else if(!selectingToy)
        {
            fDrop.SetActive(true);
            selectingFood = true;
        }
    }

    public void toyDropDown()
    {
        //If the menu is already open, close it. 
        if(tDrop.activeSelf == true)
        {
            tDrop.SetActive(false);
            pM.giveToy();
            selectingToy = false;
        }
        //If the feed menu is not opened and the button is pressed, make the menu active
        else if(!selectingFood)
        {
            tDrop.SetActive(true);
            selectingToy = true;
        }
    }

}
