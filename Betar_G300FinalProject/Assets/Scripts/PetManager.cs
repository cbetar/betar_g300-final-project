﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetManager : MonoBehaviour
{
    public GameObject pet;
    public Pet script;
    public Text notifText;
    public Text happyText;
    public Text fullText;

    private void Start()
    {
        script = pet.GetComponent<Pet>(); //Grabs pet
    }

    public void feedPet()
    {
        //Feed if pet is not full when the button is clicked.
        if(script.fullness < 10) { script.fullness++; }
        else { print("Pet is full."); }
        textModder(script.fullness, script.happiness);
        fullBar(script.fullness);
    }

    public void giveToy()
    {
        //If pet happiness is already 10, don't do anything. Otherwise, pet happiness will rise when the button is clicked.
        if(script.happiness < 10)
        { script.happiness++; }
        else { print("Your pet is extremely happy!"); }
        textModder(script.fullness, script.happiness);
        happyBar(script.happiness);
    }

    public void textModder(double full, double happy)
    {
        if(happy >= 9.00 && full >= 9.00)
        {
            notifText.text ="Your pet feels invicible, or at least that's what you gather as you watch it do stunts in the backyard.";
        } 
        else if(happy >= 9.00 && full >= 7.00)
        {
            notifText.text = "Your pet is extremely happy right now. It even cleaned the dishes, if breaking them all counts as that.";
        }
        else if (happy >= 7.00 && full >= 7.00)
        {
            notifText.text = "Your pet is feeling pretty good, so it left you a present. A slimy, barely but somehow alive one. " +
                        "Your pet seems somewhat proud.";
        }
        else if(happy >= 7.00 && full >= 4.00)
        {
            notifText.text = "It seems to be gauging whether your shoes would make a good snack or not...";
        }
        else if(happy >= 7.00 && full >= 2.00)
        {
            notifText.text = "You stumble across what looks like a half digested loafer.";
        }
        else if(happy >= 7.00 && full <= 0.00)
        {
            notifText.text = "You can hear the growl of it's stomach from far away. It would be a good idea to feed it soon, you can't afford" +
                        " to lose another finger.";
        }
        else if(happy >= 4.00 && full <= 0.00)
        {
            notifText.text = "Your pet is not only hungry but starting to feel ignored.";
        }
        else if(happy >= 2.00 && full <= 0.00)
        {
            notifText.text = "You find your pet lying in a pile of torn up belongings of yours. Next to it lies a book titled 'How to " +
                        "make murder look like an accident'. That isn't good.";
        }
        else if(happy <= 0.00 && full <= 0.00)
        {
            notifText.text = "Your neighbors leave you a very angry and somewhat threatening voicemail. It seems that your pet has resorted " +
                             "to committing arson for attention. Bad on you.";
        }
        else { print("ELSE CASE?"); }
    }

    public void happyBar(double happy)
    {
        if(happy == 10)
        {
            happyText.text = "Happiness: MAX";
        } else if (happy >= 7)
        {
            happyText.text = "Happiness: High";
        } else if (happy >= 4)
        {
            happyText.text = "Happiness: Mild";
        } else if(happy >= 1)
        {
            happyText.text = "Happiness: Low";
        } else
        {
            happyText.text = "Happiness: None";
        }
    }

    public void fullBar(double full)
    {
        if (full == 10)
        {
            fullText.text = "Fullness: Stuffed";
        }
        else if (full >= 7)
        {
            fullText.text = "Fullness: Full";
        }
        else if (full >= 4)
        {
            fullText.text = "Fullness: Peckish";
        }
        else if (full >= 1)
        {
            fullText.text = "Fullness: Hungry";
        }
        else
        {
            fullText.text = "Fullness: Ravenous";
        }
    }

}
