﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pet : MonoBehaviour
{

    public string petType;

    public PetManager pM;

    //private double MAX_FULLNESS; //The max 'fullness' value of a pet.
    public double fullness = 10.00; //The 'current' fullness of a pet.
    //private double MAX_HAPPY; //The max 'happiness' value of a pet.
    public double happiness; //The 'current' happiness of a pet.
    
    private float timePassedFullness;
    private float hungryTimePassed;

    void Awake()
    {
        timePassedFullness = Time.time;
        hungryTimePassed = Time.time;
        //hungryTimePassed = Time.time;
    }

    // Start is called before the first frame update
    void Start()
    {
        fullness = 10.00;
        happiness = 10.00;
        pM.textModder(fullness, happiness);
        pM.happyBar(happiness);
        pM.fullBar(fullness);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= timePassedFullness + 5f)
        {
            //Decrease hunger every 5 seconds
            if (fullness > 0) { 
                fullness--; //decrease fullness
            } else
            {
                print("Your pet is very hungry.");
            }
            pM.textModder(fullness, happiness);
            pM.happyBar(happiness);
            pM.fullBar(fullness);
            print("FULLNESS: " + fullness);
            timePassedFullness = Time.time;
        }
        
        //Decrease pet happiness every 10 seconds when pet is hungry.
        if(Time.time >= hungryTimePassed + 10f && fullness == 0)
        {
            if (happiness > 0)
            {
                happiness--;
            } else
            {
                print("Your pet is very unhappy.");
            }
            pM.textModder(fullness, happiness);
            pM.happyBar(happiness);
            pM.fullBar(fullness);
            print("HAPPINESS: " + happiness);
            hungryTimePassed = Time.time;
        }


    }
}
